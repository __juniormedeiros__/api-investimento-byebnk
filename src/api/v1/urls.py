from .views import ativo, aplicacao, resgate
from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter

router = SimpleRouter(trailing_slash=False)
router.register('ativo', ativo.AtivoView, basename='ativo')
router.register('aplicacao', aplicacao.AplicacaoView, basename='aplicacao')
router.register('resgate', resgate.ResgateView, basename='resgate')

urlpatterns = [
    url(r'^', include(router.urls))
]

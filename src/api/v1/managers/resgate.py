from django.db import models
from django.db.models import Sum, F
from ..models import resgate


class ResgateQuerySet(models.QuerySet):
    def saldo_resgate_aplicacao(self, pk):
        """Busca o saldo resgatado agrupado por aplicação"""
        return resgate.Resgate.objects.filter(aplicacao=pk).\
            aggregate(quantidade=Sum('quantidade_resgate'), total_resgatado=Sum('total'))

    def saldo_ativo(self, id_ativo, id_usuario):
        """Busca o saldo de resgates do usuário agrupado por ativo"""
        return resgate.Resgate.objects.filter(ativo__id=id_ativo, id_usuario=id_usuario).\
            aggregate(quantidade=Sum('quantidade_resgate'), total=Sum('total'))

from django.db import models
from django.db.models import Sum, F
from ..models import aplicacao


class AplicacaoQuerySet(models.QuerySet):
    def saldo_usuario(self, pk):
        """Busca o saldo de investimentos do usuário agrupado por aplicação"""
        return aplicacao.Aplicacao.objects.filter(usuario=pk).\
            values(id_ativo=F('ativo__id'),  nome=F('ativo__nome'), cotacao=F('ativo__cotacao')).\
            order_by('ativo__id').\
            annotate(quantidade=Sum('quantidade'), total_aplicado=Sum('total'))

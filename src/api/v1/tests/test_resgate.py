from django.test import TestCase
from django.shortcuts import resolve_url as r

from ..models.resgate import Resgate


class ResgateAPITest(TestCase):
    def setUp(self) -> None:
        self.client.post(
            r('ativo-list'),
            data={'nome': 'KNRI11', 'modalidade': 2, 'cotacao': 139.6},
            format='json'
        ).json()
        self.client.post(
            r('aplicacao-list'),
            data={'usuario': 3, 'ativo': 1, 'quantidade': 100},
            format='json'
        ).json()
        self.client.post(
            r('aplicacao-list'),
            data={'usuario': 1, 'ativo': 1, 'quantidade': 50},
            format='json'
        ).json()

    def post_save(self):
        return self.client.post(
            r('resgate-list'),
            data={'id_usuario': 3, 'aplicacao': 1, 'quantidade_resgate': 50},
            format='json'
        ).json()

    def teste_post_save(self):
        response = self.post_save()
        self.assertEqual(1, response['id'])

    def test_user_not_allowed_post_save(self):
        """Testa a validação para o usuário não resgatar as aplicações não fez"""
        response = self.client.post(
            r('resgate-list'),
            data={'id_usuario': 1, 'aplicacao': 1, 'quantidade_resgate': 50},
            format='json'
        ).json()
        self.assertEqual(response['error'], ['Usuário não permitido nesta operação!'])

    def test_rescue_value_out_post_save(self):
        """Testa a validação para o usuário não resgatar mais do que foi aplicado"""
        self.post_save()
        self.post_save()
        response = self.post_save()
        self.assertEqual(response['error'], ['Quantidade maior que o permitido!'])

    def test_rescue_value_greater_than_post_save(self):
        """Testa a validação para o usuário não resgatar mais do que foi aplicado"""
        response = self.client.post(
            r('resgate-list'),
            data={'id_usuario': 3, 'aplicacao': 1, 'quantidade_resgate': 101},
            format='json'
        ).json()
        self.assertEqual(response['error'], ['Quantidade maior que o permitido!'])

    def test_balance_resgate_aplicacao(self):
        """Testa o saldo resgatado agrupado por aplicação"""
        self.client.put(
            r('ativo-detail', 1),
            data={'nome': 'KNRI11', 'cotacao': 140.1, 'modalidade': 2},
            content_type='application/json'
        ).json()
        resgate = self.post_save()
        saldo_resgatado = Resgate.objects.saldo_resgate_aplicacao(resgate['id'])
        self.assertTrue(saldo_resgatado['total_resgatado'] == 7005.00)

    def test_balance_ativo(self):
        """Testa o saldo de resgates do usuário agrupado por ativo"""
        self.post_save()
        self.client.post(
            r('resgate-list'),
            data={'id_usuario': 1, 'aplicacao': 2, 'quantidade_resgate': 47},
            format='json'
        ).json()
        saldo_ativo = Resgate.objects.saldo_ativo(1, 3)
        self.assertTrue(saldo_ativo['total'] == 6980.00)

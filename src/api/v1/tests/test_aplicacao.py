from django.test import TestCase
from django.shortcuts import resolve_url as r
from rest_framework import status

from ..models import Aplicacao
from ..services.saldo import Saldo


class AplicacaoAPITest(TestCase):
    def setUp(self) -> None:
        self.obj = {
            'nome': 'DOGE',
            'modalidade': 3,
            'cotacao': 1.05
        }
        self.ativo = self.ativo_post_save()

    def ativo_post_save(self):
        return self.client.post(r('ativo-list'), data=self.obj, format='json').json()

    def ativo_put_save(self, value):
        self.ativo['cotacao'] = value
        return self.client.put(
            r('ativo-detail', self.ativo['id']),
            data=self.ativo,
            content_type='application/json'
        ).json()

    def aplicacao_post_save(self, usuario):
        aplicacao = {'usuario': usuario, 'ativo': self.ativo['id'], 'quantidade': 100}
        response = self.client.post(r('aplicacao-list'), data=aplicacao, format='json').json()
        return response

    def resgate_post_save(self):
        resgate = {'id_usuario': 1, 'aplicacao': 1, 'quantidade_resgate': 25}
        response = self.client.post(r('resgate-list'), data=resgate, format='json').json()
        return response

    def test_post_save(self):
        """Testa o envio da aplicação no ativo DOGE"""
        aplicacao = self.aplicacao_post_save(usuario=1)
        self.assertTrue(aplicacao['id'])

    def test_delete_not_allowed(self):
        """Testa se o método DELETE não é permitido"""
        response = self.client.delete(
            r('aplicacao-detail', self.ativo['id']),
            content_type='application/json'
        )
        self.assertTrue(status.HTTP_405_METHOD_NOT_ALLOWED, response.status_code)

    def test_balance_aplicacao_usuario_cotacao_alterado(self):
        """Testa saldo da aplicação com cotação alterada no ativo"""
        self.aplicacao_post_save(usuario=1)
        self.ativo_put_save(0.99)
        saldo = Aplicacao.objects.saldo_usuario(1)[0]
        self.assertEqual(105.00, saldo['total_aplicado'])

    def test_balance_aplicacao_usuario_with_resgate(self):
        """Testa saldo da aplicação efetuando um resgate"""
        self.aplicacao_post_save(usuario=1)
        self.ativo_put_save(0.99)
        self.resgate_post_save()
        saldo = Aplicacao.objects.saldo_usuario(1)[0]
        self.assertEqual(105.00, saldo['total_aplicado'])

    def test_aplicacao_with_diff_users(self):
        """Testa se o usuário 2 tem acesso as aplicações do usuário 1"""
        self.aplicacao_post_save(usuario=1)
        self.aplicacao_post_save(usuario=2)
        self.aplicacao_post_save(usuario=2)
        response = self.client.get(
            r('aplicacao-lista-aplicacao-usuario', 2),
            content_type='application/json'
        )
        self.assertTrue(2, len(response.data))

    def test_portfolio_balance(self):
        """Busca o saldo TOTAL das aplicações do usuário agrupado por ativo"""
        self.aplicacao_post_save(usuario=1)
        self.resgate_post_save()
        self.resgate_post_save()
        self.resgate_post_save()
        saldo_carteira = Saldo().saldo_carteira(id_usuario=1)[0]
        with self.subTest():
            self.assertEqual(100, saldo_carteira['quantidade_aplicado'])
            self.assertEqual(75, saldo_carteira['quantidade_resgatado'])
            self.assertEqual(25, saldo_carteira['quantidade_saldo'])
            self.assertEqual(78.75, saldo_carteira['total_resgatado'])
            self.assertEqual(26.25, saldo_carteira['saldo'])






import json
from django.test import TestCase
from django.shortcuts import resolve_url as r
from rest_framework import status


class AtivoAPITest(TestCase):
    def setUp(self) -> None:
        self.ativo = {
            'nome': 'DOGE',
            'modalidade': 1,
            'cotacao': 1.50000
        }

    def post_save(self):
        return self.client.post(r('ativo-list'), data=self.ativo, format='json').json()

    def test_post_save(self):
        data = self.post_save()
        self.assertTrue(data['id'])

    def test_delete_not_allowed(self):
        data = self.post_save()
        response = self.client.delete(
            r('ativo-detail', data['id']),
            content_type='application/json'
        )
        self.assertEqual(status.HTTP_405_METHOD_NOT_ALLOWED, response.status_code)

    def test_put_save(self):
        data = self.post_save()
        self.ativo['nome'] = 'BITCOIN'
        response = self.client.put(
            r('ativo-detail', data['id']),
            data=json.dumps(self.ativo),
            content_type='application/json'
        )
        self.assertEqual('BITCOIN', response.json()['nome'])

    def test_positive_cotacao_post_save(self):
        data = self.post_save()
        self.ativo['cotacao'] = -1.99
        response = self.client.put(
            r('ativo-detail', data['id']),
            data=json.dumps(self.ativo),
            content_type='application/json'
        )
        self.assertEqual('Valor inválido!', response.json()['cotacao'][0])

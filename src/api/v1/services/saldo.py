from ..models.aplicacao import Aplicacao
from ..models.resgate import Resgate


class Saldo:
    def _calcula_saldo(self, aplicacao_quantidade, resgate_quantidade, ativo_cotacao):
        saldo = 0
        if resgate_quantidade:
            saldo = (aplicacao_quantidade - resgate_quantidade) * ativo_cotacao
        return saldo

    def saldo_carteira(self, id_usuario):
        """Busca o saldo TOTAL das aplicações do usuário agrupado por ativo"""
        aplicacacoes = list(Aplicacao.objects.saldo_usuario(id_usuario))
        data = []
        for aplicacao in aplicacacoes:
            resgate = Resgate.objects.saldo_ativo(aplicacao['id_ativo'], id_usuario)
            data.append(
                {
                    "id_ativo": aplicacao['id_ativo'],
                    "nome": aplicacao['nome'],
                    "cotacao": aplicacao['cotacao'],
                    "quantidade_aplicado": aplicacao['quantidade'],
                    "total_aplicado": aplicacao['total_aplicado'],
                    "quantidade_resgatado": resgate['quantidade'] if resgate['quantidade'] else 0,
                    "total_resgatado": resgate['total'] if resgate['total'] else 0,
                    "quantidade_saldo": aplicacao['quantidade'] - resgate["quantidade"] if resgate["quantidade"] else 0,
                    "saldo": self._calcula_saldo(aplicacao['quantidade'], resgate["quantidade"], aplicacao['cotacao'])
                }
            )
        return data

from django.db import models
from .usuario import Usuario
from .ativo import Ativo
from ..managers.resgate import ResgateQuerySet
from ..validators.decimal_validator import validate_decimal


class Resgate(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_usuario = models.IntegerField(choices=Usuario.USERS, blank=False, null=False)
    aplicacao = models.ForeignKey(
        'Aplicacao',
        on_delete=models.CASCADE,
        db_column='id_aplicacao',
        related_name='resgate_aplicacao',
        blank=False,
        null=False)
    ativo = models.ForeignKey(
        'Ativo',
        on_delete=models.CASCADE,
        db_column='id_ativo',
        related_name='resgate_ativo',
        blank=False,
        null=False)
    cotacao_aplicacao = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        blank=False,
        null=False,
        validators=[validate_decimal]
    )
    cotacao_ativo = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        blank=False,
        null=False,
        validators=[validate_decimal]
    )
    quantidade_resgate = models.PositiveIntegerField(blank=False, null=False)
    endereco_ip = models.GenericIPAddressField(blank=False, null=False)
    total = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        blank=False,
        null=False,
        validators=[validate_decimal]
    )
    data_resgate = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    objects = ResgateQuerySet.as_manager()

    def save(self, *args, **kwargs):
        self.cotacao_aplicacao = self.aplicacao.cotacao_ativo
        self.ativo = Ativo.objects.get(pk=self.aplicacao.ativo.id)
        self.cotacao_ativo = self.ativo.cotacao
        # Calculado do total resgatado considerando lucro e prejuízo
        self.total = (self.quantidade_resgate * self.cotacao_aplicacao) + \
                     (self.quantidade_resgate * self.cotacao_ativo) - \
                     (self.quantidade_resgate * self.cotacao_aplicacao)
        super(Resgate, self).save(*args, **kwargs)

    class Meta:
        db_table = 'resgates'
        app_label = 'v1'
        managed = True
        ordering = ['id']

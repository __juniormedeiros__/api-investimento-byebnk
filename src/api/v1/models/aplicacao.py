from django.db import models
from .usuario import Usuario
from ..managers.aplicacao import AplicacaoQuerySet
from ..validators.decimal_validator import validate_decimal


class Aplicacao(models.Model):
    id = models.BigAutoField(primary_key=True)
    usuario = models.IntegerField(db_column='id_usuario', choices=Usuario.USERS, blank=False, null=False)
    ativo = models.ForeignKey(
        'Ativo',
        on_delete=models.CASCADE,
        db_column='id_ativo',
        related_name='aplicacao_ativo',
        blank=False,
        null=False)
    cotacao_ativo = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        blank=False,
        null=False,
        validators=[validate_decimal]
    )
    quantidade = models.PositiveIntegerField()
    total = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        blank=False,
        null=False,
        validators=[validate_decimal]
    )
    endereco_ip = models.GenericIPAddressField(blank=False, null=False)
    data_aplicacao = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    objects = AplicacaoQuerySet.as_manager()

    def save(self, *args, **kwargs):
        self.cotacao_ativo = self.ativo.cotacao
        self.total = self.cotacao_ativo * self.quantidade
        super(Aplicacao, self).save(*args, **kwargs)

    class Meta:
        db_table = 'aplicacoes'
        app_label = 'v1'
        managed = True
        ordering = ['id']

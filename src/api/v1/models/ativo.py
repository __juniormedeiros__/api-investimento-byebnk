from django.db import models
from ..validators.decimal_validator import validate_decimal


class Ativo(models.Model):
    MODALIDADE = (
        (1, 'RENDA_FIXA'),
        (2, 'RENDA_VARIAVEL'),
        (3, 'CRIPO')
    )
    id = models.BigAutoField(primary_key=True)
    nome = models.CharField(unique=True, max_length=50, blank=False, null=False)
    modalidade = models.IntegerField(blank=False, null=False, choices=MODALIDADE)
    cotacao = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        blank=False,
        null=False,
        validators=[validate_decimal]
    )
    data_cadastro = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    def __str__(self):
        return self.nome

    @staticmethod
    def get_tuple_value(value):
        return Ativo.MODALIDADE[value-1][1]

    class Meta:
        db_table = 'ativos'
        app_label = 'v1'
        managed = True
        ordering = ['id']

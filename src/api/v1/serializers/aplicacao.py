from rest_flex_fields import FlexFieldsModelSerializer
from rest_framework import serializers
from ..models.aplicacao import Aplicacao


class AplicacaoSerializer(FlexFieldsModelSerializer):
    nome_ativo = serializers.ReadOnlyField(source='ativo.nome')
    nome_usuario = serializers.SerializerMethodField()

    class Meta:
        model = Aplicacao
        fields = ('id', 'usuario', 'nome_usuario',  'ativo', 'nome_ativo', 'quantidade', 'cotacao_ativo', 'total', 'data_aplicacao')
        read_only_fields = ('id', 'total', 'cotacao_ativo', 'data_aplicacao')

    def get_nome_usuario(self, obj):
        return obj.get_usuario_display()

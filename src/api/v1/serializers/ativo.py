from rest_flex_fields import FlexFieldsModelSerializer
from rest_framework import serializers
from ..models.ativo import Ativo


class AtivoSerializer(FlexFieldsModelSerializer):
    nome_modalidade = serializers.SerializerMethodField()

    def get_nome_modalidade(self, obj):
        return obj.get_modalidade_display()

    class Meta:
        model = Ativo
        fields = ('id', 'nome', 'modalidade', 'nome_modalidade', 'cotacao', 'data_cadastro')
        read_only_fields = ('id', 'data_aplicacao')
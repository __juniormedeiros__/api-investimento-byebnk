from rest_flex_fields import FlexFieldsModelSerializer
from rest_framework import serializers
from ..models.resgate import Resgate
from ..models.aplicacao import Aplicacao


class ResgateSerializer(FlexFieldsModelSerializer):
    cotacao_aplicacao = serializers.ReadOnlyField()
    cotacao_ativo = serializers.ReadOnlyField()
    id_ativo = serializers.ReadOnlyField(source='ativo.id')
    nome_ativo = serializers.ReadOnlyField(source='ativo.nome')
    nome_usuario = serializers.SerializerMethodField()

    class Meta:
        model = Resgate
        fields = ('id', 'id_usuario', 'nome_usuario', 'aplicacao', 'id_ativo', 'nome_ativo', 'cotacao_aplicacao', 'cotacao_ativo',
                  'quantidade_resgate', 'total')
        read_only_fields = ('id', 'data_resgate', 'total')

    def get_nome_usuario(self, obj):
        return obj.get_id_usuario_display()

    def validate(self, data):
        super().validate(data)
        aplicacao = Aplicacao.objects.get(pk=data['aplicacao'].id)
        if not aplicacao.usuario == data['id_usuario']:
            raise serializers.ValidationError({'error': 'Usuário não permitido nesta operação!'})

        resgates = Resgate.objects.saldo_resgate_aplicacao(aplicacao.id)
        if resgates['quantidade'] and \
                (resgates['quantidade'] + data['quantidade_resgate']) > aplicacao.quantidade:
            raise serializers.ValidationError({'error': 'Quantidade maior que o permitido!'})
        elif data['quantidade_resgate'] > aplicacao.quantidade:
            raise serializers.ValidationError({'error': 'Quantidade maior que o permitido!'})
        return data

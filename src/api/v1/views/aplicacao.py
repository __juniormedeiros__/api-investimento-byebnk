from rest_framework import viewsets, serializers
from rest_flex_fields.views import FlexFieldsMixin
from rest_framework.response import Response
from rest_framework.decorators import action
from ..models.aplicacao import Aplicacao
from ..serializers.aplicacao import AplicacaoSerializer
from ..utils.util import get_client_ip
from ..services.saldo import Saldo


class AplicacaoView(FlexFieldsMixin, viewsets.ModelViewSet):
    queryset = Aplicacao.objects.all()
    serializer_class = AplicacaoSerializer
    http_method_names = ['get', 'post']

    def perform_create(self, serializer):
        serializer.save(endereco_ip=get_client_ip(self.request))

    def list(self, request, *args, **kwargs):
        raise serializers.ValidationError('Não permitido!')

    def retrieve(self, request, *args, **kwargs):
        raise serializers.ValidationError('Não permitido!')

    @action(methods=['get'], detail=True, url_path='usuario')
    def lista_aplicacao_usuario(self, request, pk):
        try:
            if pk.isnumeric():
                aplicacoes = Aplicacao.objects.filter(usuario=pk)
                serializer = AplicacaoSerializer(aplicacoes, many=True)
                return Response(serializer.data)
            else:
                return Response([])
        except Exception as e:
            raise serializers.ValidationError(f'Erro ao buscar aplicações!: {e}')

    @action(methods=['get'], detail=True, url_path='usuario-saldo')
    def lista_saldo_usuario(self, request, pk):
        try:
            if pk.isnumeric():
                results = Saldo().saldo_carteira(pk)
                return Response(results)
            else:
                return Response([])
        except Exception as e:
            raise serializers.ValidationError(f'Erro ao buscar aplicações!: {e}')

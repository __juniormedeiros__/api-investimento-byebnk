from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import serializers
from rest_flex_fields.views import FlexFieldsMixin
from ..models.ativo import Ativo
from ..serializers.ativo import AtivoSerializer


class AtivoView(FlexFieldsMixin, viewsets.ModelViewSet):
    queryset = Ativo.objects.all()
    serializer_class = AtivoSerializer
    http_method_names = ['get', 'post', 'put', 'patch']

    @action(methods=['get'], detail=True, url_path='modalidade')
    def lista_ativo_modalidade(self, request, pk):
        try:
            ativos = Ativo.objects.filter(modalidade=pk)
            if ativos:
                serializer = AtivoSerializer(ativos, many=True)
                return Response(serializer.data)
            else:
                return Response({})
        except Exception as e:
            raise serializers.ValidationError(f'Erro ao buscar aplicações!: {e}')


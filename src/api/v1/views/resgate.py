from rest_framework import viewsets, serializers
from rest_flex_fields.views import FlexFieldsMixin
from rest_framework.response import Response
from rest_framework.decorators import action
from ..models.resgate import Resgate
from ..serializers.resgate import ResgateSerializer
from ..utils.util import get_client_ip


class ResgateView(FlexFieldsMixin, viewsets.ModelViewSet):
    queryset = Resgate.objects.all()
    serializer_class = ResgateSerializer
    http_method_names = ['get', 'post', 'delete']

    def perform_create(self, serializer):
        serializer.save(endereco_ip=get_client_ip(self.request))

    def list(self, request, *args, **kwargs):
        raise serializers.ValidationError('Não permitido!')

    def retrieve(self, request, *args, **kwargs):
        raise serializers.ValidationError('Não permitido!')

    @action(methods=['get'], detail=True, url_path='usuario')
    def lista_aplicacao_usuario(self, request, pk):
        try:
            if pk.isnumeric():
                resgates = Resgate.objects.filter(id_usuario=pk)
                serializer = ResgateSerializer(resgates, many=True)
                return Response(serializer.data)
            else:
                return Response([])
        except Exception as e:
            raise serializers.ValidationError(f'Erro ao buscar resgates!: {e}')

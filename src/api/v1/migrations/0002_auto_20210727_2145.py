# Generated by Django 3.2.5 on 2021-07-28 00:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('v1', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aplicacao',
            name='quantidade',
            field=models.PositiveIntegerField(),
        ),
        migrations.AlterField(
            model_name='resgate',
            name='quantidade_resgate',
            field=models.PositiveIntegerField(),
        ),
    ]

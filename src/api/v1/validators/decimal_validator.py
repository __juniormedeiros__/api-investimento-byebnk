from rest_framework.exceptions import ValidationError


def validate_decimal(value):
    if value <= 0:
        raise ValidationError('Valor inválido!')
    return value

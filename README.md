# API Investimentos Byebnk

Principais dependências:

![](https://img.shields.io/badge/Django-3.2.5-critical)
![](https://img.shields.io/badge/djangorestframework-3.12-important)
![](https://img.shields.io/badge/Jinja2-3.0.1-success)
![](https://img.shields.io/badge/requests-2.26.0-critical)
![](https://img.shields.io/badge/drf__flex__fields-0.9.1-informational)
![](https://img.shields.io/badge/gunicorn-20.1.0-critical)

> Este projeto foi criado de acordo com as especificações do [**Gitlab**](https://gitlab.com/byebnk/desafio-backend)

A API de investimentos da Byebnk tem como o intuito servir como backend para aplicações financeiras de investimentos. 

## Algumas funcionalidades 
- Cadastro de **ativos** para investimentos `` api-investimentos/v1/ativo``
- **Aplicações** em ativos de investimentos  `` api-investimentos/v1/aplicacao``
- **Resgates** de aplicações `` api-investimentos/v1/resgate``

## Ambiente de desenvolvimento

Pré-requisitos:

- Python na versão especificada no projeto, baixar e instalar em:
  
> [Windows](https://www.python.org/downloads/windows/)

> [Mac](https://www.python.org/downloads/mac-osx/)

> [Linux](https://www.python.org/downloads/source/)


Após fazer o clone do projeto por ssh ou https: 

- ssh: ``git clone git@bitbucket.org:juniormedeiros/api-investimentos-byebnk.git``
- https: ``git clone https://seu_usuario@bitbucket.org/juniormedeiros/api-investimentos-byebnk.git``

Acessar o fonte projeto:

    $ cd api-investimento-byebnk

Instalação da virtualenv:

[https://virtualenv.pypa.io/en/latest/installation.html](https://virtualenv.pypa.io/en/latest/installation.html)

Criação da virtualenv para isolar a instalação das dependências:

    $ virtualenv .venv

Ativação da virtualenv:

    $ source .venv/bin/activate

Realizar a instalação das dependências do projeto:

    $ pip install -r src/requirements.txt

Configurar as variáveis de ambiente para desenvolvimento:

| Ambientes              | Descrição                                            |
| ---------------------- | --------------------------------------------------   |
| PRODUCTION             | Define o uso do API em ambiente de Produção          |
| SANDBOX                | Define o uso da API em ambiente de Testes            |
| DEVELOPMENT            | Define o uso da API em ambiente de Desenvolvimento   |


**IMPORTANTE**: Para o ambientes de produção é utilizado o arquivo  ***src/deploy/envs*** [production.yml] que
define as configurações de variáveis de ambiente e a instância do container DOCKER que executará.

### Ambiente

| Variável    | Descrição                          |
| ----------- | ---------------------------------- |
| ENVIRONMENT | DEVELOPMENT, SANDBOX ou PRODUCTION |

Antes de subir a aplicação é necessário gerar as migrations para criação do banco de dados conforme o 
tipo de **ambiente** escolhido.

    $ python manage.py makemigrations --settings settings.<ENVIRONMENT>

Aplicando as migrações a base de dados escolhida.

    $ python manage.py migrate --settings settings.<ENVIRONMENT>

Neste momento, já se pode rodar o comando abaixo para executar o projeto no ambiente desejado:

    $ python manage.py runserver --settings settings.<ENVIRONMENT>

### Estrutura do projeto

Estrutura de diretórios / arquivos:

```
.
├── README.md
├── .gitignore
├── deploy/
│   ├── run_api.sh
│   ├── envs/
│   │   └── production.yml
│   └── images/
│       └── Dockerfile
├── doc/
│   ├── DiagramaBaseDados.png
│   ├── DiagramaSequencia.png
│   └── DiagramaSequencia.txt
└── src/
    ├── __init__.py
    ├── manage.py
    ├── settings/
    │   ├── __init__.py
    │   ├── development.py
    │   ├── production.py
    │   └── sandbox.py
    ├── requirements.txt
    └── api/
        ├── __init__.py
        ├── asgi.py
        ├── urls.py
        ├── wsgi.py
        └── v1/
            ├── managers/
            ├── migrations/
            ├── models/
            ├── serializers/
            ├── services/
            ├── tests/
            ├── utils/
            ├── validators/
            └── views/      
```

### Lista de EndPoints:

- api-investimento/v1/ativo
- api-investimento/v1/aplicacao
- api-investimento/v1/resgate
- api-investimento/v1/ativo/**<id_modalidade>**/modalidade
- api-investimento/v1/aplicacao/**<id_usuario>**/usuario
- api-investimento/v1/aplicacao/**<id_usuario>**/usuario-saldo
- api-investimento/v1/resgate/**<id_usuario>**/usuario

### Macro do processo

a) EndPoint /api-investimento/v1/ativo - responsável pela listagem e inclusão de ativos:

``` python
    {
       "nome": "DOGE"
       "modalidade": [
            (1, 'RENDA_FIXA'),
            (2, 'RENDA_VARIAVEL'),
            (3, 'CRIPO')],
       "cotacao" 100
    }
```

- Inclui os ativos para investimentos, o ativo cadastrado pode ser utilizado por qualquer usuário.
  > OBS: O cadastro de usuário não faz parte do escopo deste projeto, por isso os usuários
  > estão pré cadastrados no sistema { "User 1": 1, "User 2": 2, "User 3": 3 } 
- Lista todos os ativos cadastrados independente do usuário que realizou o cadastro.
- Altera o nome e a cotação do ativo;

b) EndPoint /api-investimento/v1/ativo/<id_modalidade>/modalidade 

- responsável pela listagem dos ativos cadastrados por modalidade:

``` python
    (
        (1, 'RENDA_FIXA'),
        (2, 'RENDA_VARIAVEL'),
        (3, 'CRIPO')
    )
```

c) EndPoint **/api-investimento/v1/aplicacao** 

``` python
    {
       "usuario": [
            (1, "User 1"),
            (2, "User 2"),
            (3, "User 3")],
       "ativo": 1
       "quantidade" 100
    }
```

- Inclui as aplicações de investimentos por ativo;


d) EndPoint **/api-investimentos/v1/aplicacao/<id_usuario>/usuario** 

- Responsável pela listagem de aplicações por usuário:

e) EndPoint **/api-investimentos/v1/aplicacao/<id_usuario>/usuario-saldo** 

- Responsável por buscar o saldo de aplicações por usuário, conforme as aplicações e resgates;

f) EndPoint **/api-investimento/v1/resgate/<id_usuario>/usuario**

- Responsável pela listagem de resgates por usuário:
``` python
[
    {
        "id": 3,
        "id_usuario": 3,
        "nome_usuario": "User 3",
        "aplicacao": 5,
        "id_ativo": 4,
        "nome_ativo": "CDB",
        "cotacao_aplicacao": 100.0,
        "cotacao_ativo": 99.87,
        "quantidade_resgate": 25,
        "total": "2496.7500000000"
    },
    {
        "id": 5,
        "id_usuario": 3,
        "nome_usuario": "User 3",
        "aplicacao": 5,
        "id_ativo": 4,
        "nome_ativo": "CDB",
        "cotacao_aplicacao": 100.0,
        "cotacao_ativo": 101.0,
        "quantidade_resgate": 20,
        "total": "2020.0000000000"
    }
]
```

Diagrama de sequência:

![](doc/DiagramaSequencia.png)

Diagrama da Base de Dados:

![](doc/DiagramaBaseDados.png)

### Release / Deploy

Após configurar o projeto você deve primeiro gerar a imagem Docker **image-investimento:latest**:

    $ docker build -t image-investimento -f deploy/images/Dockerfile .

Com a imagem gerada você pode executar o docker-compose para iniciar o container com nome **api-investimento**:

    $ docker-compose -f deploy/envs/production.yml up

Nesse momento a aplicação deve iniciar na porta 8000 do servidor.


### Request / Collections

Para facilitar a manutenção ou utilização dos EndPoint abaixo segue as collections do Postman.

DEFINIR 

### Documentação

Dentro da pasta Docs é possível visualizar arquivos com as documentações:

- [Diagrama de Sequências](docs/DiagramaSequencia.png)

#### Para o suporte

DEFINIR

#### Para o cliente

DEFINIR